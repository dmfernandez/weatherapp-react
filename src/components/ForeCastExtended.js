import React from 'react';
import PropTypes from 'prop-types';
import ForecastItem from './ForecastItem';

import './styles.css';

const renderForecastItemDays = (forecastData) => {
    return forecastData.map(forecast => (
        <ForecastItem
            key={`${forecast.weekDay}${forecast.hour}`}
            weekDay={forecast.weekDay}
            hour={forecast.hour}
            data={forecast.data}>
        </ForecastItem>));
}

const renderProgres = () => {
    return <h3>Cargando...</h3>;
}


const ForeCastExtended = ({ city, forecastData }) => (
    <div>
        <h2 className='formcast-title'>Pronóstico Extendido para {city}</h2>
        {forecastData ? renderForecastItemDays(forecastData) : renderProgres()}
    </div>
);

ForeCastExtended.protoType = {
    city: PropTypes.string.isRequired,
    forecastData: PropTypes.array.isRequired,
    
}

export default ForeCastExtended;