import React from 'react';
// import PropTypes from 'prop-types';
import WeatherExtraInfo from './WeatherExtraInfo';
import WeatherTemperature from './WeatherTemperature';

// import {
//     CLOUD,
//     CLOUDY,
//     SUN,
//     RAIN,
//     SNOW,
//     WINDY
// } from './../../constants/weathers'
import './styles.css'

const WeatherData = ({ data: { temperature, weatherState, humidity, wind } }) => {

    // const {city, data} = this.data;

    return (
        <div className="weatherDataCont">
            <WeatherTemperature temperature={temperature} weatherState={weatherState}></WeatherTemperature>
            <WeatherExtraInfo humidity={humidity} wind={wind}></WeatherExtraInfo>
        </div>
    );
};


export default WeatherData;