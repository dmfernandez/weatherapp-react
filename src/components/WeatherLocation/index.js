import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import getUrlWeatherByCity from './../../services/getUrlWeatherByCity';
import CircularProgress from '@material-ui/core/CircularProgress'
import transformWeather from './../../services/transformWeather';
import Location from './Location';
import WeatherData from './WeatherData';
import './styles.css'



const data = {
    temperature: 7,
    weatherState: '',
    humidity: 11,
    wind: '21 m/s'
}

class WeatherLocation extends Component {

    constructor(props) {
        super(props);
        const { city } = props;

        this.state = {
            city: city,
            data: data
        };
    }

    componentDidMount() {
        console.log("componentDidMount");
        this.handleUpdateClick();
    }

    componentDidUpdate(prevProps, prevState) {
        console.log("componentDidUpdate");
    }


    handleUpdateClick = () => {

        const api_weather = getUrlWeatherByCity(this.state.city);

        fetch(api_weather).then(resolve => {
            console.log(resolve);
            return resolve.json();
        }).then(data => {
            const newWeather = transformWeather(data);
            console.log(newWeather);
            this.setState({ data: newWeather });
        });


    }

    render() {
        
        const { onWeatherLocationClick } = this.props;
        // const { city, data } = this.data;

        return (
            <div className="weatherLocationCont" onClick={onWeatherLocationClick}>
                <Location city={this.state.city}></Location>
                {data ? <WeatherData data={this.state.data}></WeatherData> : <CircularProgress />}
            </div>
        );
    }
};

WeatherLocation.propTypes = {
    city: PropTypes.string.isRequired,
}

export default WeatherLocation;