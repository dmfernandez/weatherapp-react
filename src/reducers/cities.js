import { SET_FORECAST_CITY } from '../actions';

export const cities = (state = {}, action) => {

    switch (action.type) {
        case SET_FORECAST_CITY:
            const { city, forecastData } = action.payload;
            return { ...state, [city]: { forecastData } }

        default:
            return state;
    }
}